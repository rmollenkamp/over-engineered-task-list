# Over-Engineered Task List
Thank you for considering me as a condidate. This application that you have possession of was written by me, Roy Mollenkamp, as an example of the way I like to code. It is a simple task list application, but it shows a few different coding/Angular concepts, such as: handling Observables, dealing with forms (both reactive and template-driven), using services, handling errors in database calls, etc. Please feel free to peruse the code and let me know if you have any questions.

## General Information
This application uses a nodeJs backend, and a basic JSON database stored in their respective folders

### JSON Database
The json data in the `db` module will act as our database.
Inside the people.json file, you will find a list of people objects. This represents the people that we will be using to create tasks for.
Inside the tasks.json file, you will find a list of tasks, and those tasks will have a personId which assigns them to a particular person.

### NodeJs API
`api` has been built using an express server with several basic routes to interact with the json database.
Info on how to start the api server is in the api > README.md file.

### Angular UI
The `ui` module is where all of the front end logic resides for the actual Over-Engineered Task List application
Info on how to start the ui server is in the ui > README.md file.