import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { PersonService } from 'src/app/_services/person.service';
import { BrowserTestingModule } from '@angular/platform-browser/testing';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { PersonSelectComponent } from './person-select.component';

import { people } from '../_testing/people';
import { By } from '@angular/platform-browser';
import { ReplaySubject, of, tap } from 'rxjs';

describe('PersonSelectComponent', () => {
  let fixture: ComponentFixture<PersonSelectComponent>;
  let component: PersonSelectComponent;

  const people$ = new ReplaySubject();
  const selectedPerson$ = new ReplaySubject();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        BrowserTestingModule,
        NoopAnimationsModule,
        MatButtonModule,
        MatSelectModule
      ],
      providers: [
        {
          provide: PersonService,
          useValue: { people$, selectedPerson$, getPeople: () => {} }
        }
      ],
      declarations: [
        PersonSelectComponent
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents()
    .then(() => {
      fixture = TestBed.createComponent<PersonSelectComponent>(PersonSelectComponent);
      component = fixture.componentInstance;
      people$.next(people);
      selectedPerson$.next(people[0]);
      fixture.detectChanges();
    });
  }));

  it('should create', waitForAsync(() => {
    expect(component).toBeTruthy();
  }));

  it('should return three users from the person service', waitForAsync(() => {
    fixture.whenStable()
    .then(() => {
      let selectElement = fixture.debugElement.query(By.css('.person-select')).nativeElement;
      selectElement.click();
      fixture.detectChanges();
      fixture.whenStable()
      .then(() => {
        let options = fixture.debugElement.queryAll(By.css('.person-option'))
        expect(options.length).toEqual(3);
      });
    });
  }));

  it('should select a person from the list', waitForAsync(() => {
    fixture.whenStable()
    .then(() => {
      let selectElement = fixture.debugElement.query(By.css('.person-select')).nativeElement;
      selectElement.click();
      selectElement.dispatchEvent(new Event('click'));
      fixture.detectChanges();
      let options = fixture.debugElement.queryAll(By.css('.person-option'))
      options[0].nativeElement.click();
      options[0].nativeElement.dispatchEvent(new Event('click'));
      selectElement.dispatchEvent(new Event('change'));
      fixture.detectChanges();
      expect(selectElement.innerHTML).toContain('Test Person 1');
    });
  }));

  it('should change the current person', waitForAsync(() => {
    component.changeCurrentPerson(people[0]);
    selectedPerson$
    .pipe(
      tap((person) => expect(person).toEqual(people[0]))
    ).subscribe();
  }));
});
