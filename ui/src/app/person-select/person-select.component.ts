import { Component, OnInit } from "@angular/core";
import { Observable, pipe, take, tap } from "rxjs";
import { Person } from "src/app/_models/person";
import { PersonService } from "src/app/_services/person.service";

@Component({
  selector: 'app-person-select',
  templateUrl: './person-select.component.html',
  styleUrls: ['./person-select.component.css'],
})
export class PersonSelectComponent implements OnInit {
  constructor(
    protected personService: PersonService
  ) { /* TODO document why this constructor is empty */ }

  ngOnInit(): void {
    this.personService.getPeople();
  }

  changeCurrentPerson(person: Person): void{
    this.personService.selectedPerson$.next(person);
  }
}
