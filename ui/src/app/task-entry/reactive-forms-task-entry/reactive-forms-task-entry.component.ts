import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef } from "@angular/material/dialog";
import { concatMap, take, tap } from "rxjs";
import { TaskItem } from "src/app/_models/task";
import { PersonService } from "src/app/_services/person.service";
import { TaskServices } from "src/app/_services/task.services";

@Component({
  selector: 'app-reactive-forms-task-entry',
  templateUrl: './reactive-forms-task-entry.component.html',
  styleUrls: ['./reactive-forms-task-entry.component.css'],
})
export class ReactiveFormsTaskEntryComponent implements OnInit {
  task = new TaskItem();
  form = new FormGroup({
    description: new FormControl('',{validators: [Validators.required, Validators.pattern('^([A-Z])\\w+:\\s.+$')]}),
    status: new FormControl('', {validators: [Validators.required]})
  });

  constructor(
    private taskService: TaskServices,
    private personService: PersonService,
    public dialogRef: MatDialogRef<ReactiveFormsTaskEntryComponent>
  ) { /* TODO document why this constructor is empty */ }

  ngOnInit(): void {
      this.personService.selectedPerson$
      .pipe(
        tap((person) => this.task.personId = person.id)
      ).subscribe();
  }

  /* istanbul ignore next */
  closeDialog(): void {
    this.dialogRef.close();
  }

  submitTask(): void{
    this.task.description = this.form.controls.description.value;
    this.task.status = this.form.controls.status.value;
    this.taskService.tasks$
    .pipe(
      take(1),
      tap((tasks) => {
        this.task.id = tasks.length + 1
      }),
      tap(() => this.taskService.saveTask(this.task)),
      tap(() => {
        this.dialogRef.close();
      })
    ).subscribe();
  }
}
