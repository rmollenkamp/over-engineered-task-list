import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TaskServices } from 'src/app/_services/task.services';
import { PersonService } from 'src/app/_services/person.service';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { BrowserTestingModule } from '@angular/platform-browser/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { ReactiveFormsTaskEntryComponent } from './reactive-forms-task-entry.component';
import { By } from '@angular/platform-browser';

import { people } from 'src/app/_testing/people';
import { taskItems } from 'src/app/_testing/tasks';
import { ReplaySubject, firstValueFrom, of } from 'rxjs';
import { TaskItem } from 'src/app/_models/task';
import { Person } from 'src/app/_models/person';


describe('ReactiveFormsTaskEntryComponent', () => {
  let fixture: ComponentFixture<ReactiveFormsTaskEntryComponent>;
  let component: ReactiveFormsTaskEntryComponent;

  const tasks$ = new ReplaySubject<TaskItem[]>(1);
  const people$ = new ReplaySubject<Person[]>(1);
  const selectedPerson$ = new ReplaySubject<Person>(1);
  const savedTask$: ReplaySubject<TaskItem> = new ReplaySubject<TaskItem>(1);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        BrowserTestingModule,
        NoopAnimationsModule,
        MatDialogModule,
        MatInputModule,
        MatButtonModule,
        MatSelectModule,
        ReactiveFormsModule
      ],
      providers: [
        {
          provide: TaskServices,
          useValue: { tasks$, getTasks: () => {}, saveTask: (task: TaskItem) => { savedTask$.next(task); return of('Ok') } }
        },
        {
          provide: PersonService,
          useValue: { people$, selectedPerson$, getPeople: () => {} }
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {}
        },
        {
          provide: MatDialogRef,
          useValue: { close: () => {} }
        }
      ],
      declarations: [
        ReactiveFormsTaskEntryComponent
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents()
    .then(() => {
      fixture = TestBed.createComponent<ReactiveFormsTaskEntryComponent>(ReactiveFormsTaskEntryComponent);
      component = fixture.componentInstance;
      people$.next(people);
      selectedPerson$.next(people[0]);
      tasks$.next(taskItems);
      fixture.detectChanges();
    });
  }));

  beforeEach(() => {
  });

  it('should create', waitForAsync(() => {
    expect(component).toBeTruthy();
  }));

  it('should display the task description text box', waitForAsync(() => {
    fixture.whenStable()
    .then(() => {
      expect(fixture.debugElement.query(By.css('.task-description'))).toBeTruthy();
    });
  }));

  it('should set the value of the description when entered', waitForAsync(() => {
    fixture.whenStable()
    .then(() => {
      let testValue = 'Testing: Something';
      let descriptionInput = fixture.debugElement.query(By.css('.task-description')).nativeElement;
      descriptionInput.value = testValue;
      descriptionInput.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(component.form.controls.description.value).toEqual(testValue);
    });
  }));

  it('should show an error if the value entered is invalid', waitForAsync(() => {
    fixture.whenStable()
    .then(() => {
      let testValue = 'Testing';
      let descriptionInput = fixture.debugElement.query(By.css('.task-description')).nativeElement;
      descriptionInput.value = testValue
      descriptionInput.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(descriptionInput).toHaveClass('ng-invalid');
    });
  }));

  it('should not show an error if the value entered is valid', waitForAsync(() => {
    fixture.whenStable()
    .then(() => {
      let testValue = 'Testing: Something';
      let descriptionInput = fixture.debugElement.query(By.css('.task-description')).nativeElement;
      descriptionInput.value = testValue
      descriptionInput.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(descriptionInput).not.toHaveClass('ng-invalid');
    });
  }));

  it('should submit the task', waitForAsync(() => {
    fixture.whenStable()
    .then(() => {
      let descriptionValue = 'Testing';
      let statusValue = 'incomplete';
      let descriptionInput = fixture.debugElement.query(By.css('.task-description')).nativeElement;
      let statusInput = fixture.debugElement.query(By.css('.task-status')).nativeElement;
      let submitButton = fixture.debugElement.query(By.css('.submit-button')).nativeElement;
      descriptionInput.value = descriptionValue;
      descriptionInput.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      statusInput.click();
      fixture.detectChanges();
      let statusOption = fixture.debugElement.query(By.css('.option-one')).nativeElement;
      statusOption.click();
      fixture.detectChanges();
      submitButton.click();
      submitButton.dispatchEvent(new Event('click'));
      fixture.detectChanges();
      expect(component.task.description).toEqual(descriptionValue);
      expect(component.task.status).toEqual(statusValue);
    });
  }));

  it('should save the task', waitForAsync(() => {
    let descriptionValue = "Testing 1";
    let statusValue = 'incomplete';
    component.form.controls.description.setValue(descriptionValue);
    component.form.controls.status.setValue(statusValue);
    component.task.personId = 1;
    component.submitTask();
    let expectedTaskItem = new TaskItem();
    expectedTaskItem.description = descriptionValue;
    expectedTaskItem.id = 4;
    expectedTaskItem.status = statusValue;
    expectedTaskItem.personId = 1;
    expectAsync(firstValueFrom(savedTask$)).toBeResolvedTo(expectedTaskItem);
  }));
});
