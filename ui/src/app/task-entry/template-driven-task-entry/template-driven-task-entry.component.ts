import { Component, OnInit } from "@angular/core";
import { MatDialogRef } from "@angular/material/dialog";
import { concatMap, take, tap } from "rxjs";
import { TaskItem } from "src/app/_models/task";
import { PersonService } from "src/app/_services/person.service";
import { TaskServices } from "src/app/_services/task.services";

@Component({
  selector: 'app-template-driven-task-entry',
  templateUrl: './template-driven-task-entry.component.html',
  styleUrls: ['./template-driven-task-entry.component.css'],
})
export class TemplateDrivenTaskEntryComponent implements OnInit {
  newTask: TaskItem = new TaskItem();

  constructor(
    private taskService: TaskServices,
    private personService: PersonService,
    public dialogRef: MatDialogRef<TemplateDrivenTaskEntryComponent>
  ) { /* TODO document why this constructor is empty */ }

  ngOnInit(): void {
    this.getSelectedPerson();
  }

  getSelectedPerson(): void {
    this.personService.selectedPerson$
    .pipe(
      take(1),
      tap((person) => {
        this.newTask.personId = person.id
      })
    ).subscribe();
  }

  submitTask(): void {
    this.taskService.tasks$
    .pipe(
      take(1),
      tap((tasks) => {
        this.newTask.id = tasks.length + 1
      }),
      tap(() => this.taskService.saveTask(this.newTask)),
      tap(() => {
        this.dialogRef.close();
      })
    ).subscribe();
  }

  /* istanbul ignore next */
  closeDialog(): void {
    this.dialogRef.close();
  }
}
