import { ComponentFixture, TestBed, fakeAsync, flush, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TaskServices } from 'src/app/_services/task.services';
import { TemplateDrivenTaskEntryComponent } from './template-driven-task-entry.component';
import { PersonService } from 'src/app/_services/person.service';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { BrowserTestingModule } from '@angular/platform-browser/testing';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { Person } from 'src/app/_models/person';
import { ReplaySubject, firstValueFrom, of } from 'rxjs';
import { By } from '@angular/platform-browser';
import { taskItems } from 'src/app/_testing/tasks';
import { people } from 'src/app/_testing/people';
import { TaskItem } from 'src/app/_models/task';

describe('TemplateDrivenTaskEntryComponent', () => {
  let fixture: ComponentFixture<TemplateDrivenTaskEntryComponent>;
  let component: TemplateDrivenTaskEntryComponent;

  const tasks$ = new ReplaySubject<TaskItem[]>(1);
  const people$ = new ReplaySubject<Person[]>(1);
  const selectedPerson$ = new ReplaySubject<Person>(1);
  const savedTask$: ReplaySubject<TaskItem> = new ReplaySubject<TaskItem>(1);


  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        BrowserTestingModule,
        NoopAnimationsModule,
        MatDialogModule,
        MatInputModule,
        MatButtonModule,
        MatFormFieldModule,
        MatSelectModule,
        FormsModule
      ],
      providers: [
        {
          provide: TaskServices,
          useValue: { tasks$, getTasks: () => {}, saveTask: (task: TaskItem) => { savedTask$.next(task); return of('Ok') } }
        },
        {
          provide: PersonService,
          useValue: { people$, selectedPerson$, getPeople: () => {} }
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {}
        },
        {
          provide: MatDialogRef,
          useValue: { close: () => {} }
        }
      ],
      declarations: [
        TemplateDrivenTaskEntryComponent
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents()
    .then(() => {
      fixture = TestBed.createComponent<TemplateDrivenTaskEntryComponent>(TemplateDrivenTaskEntryComponent);
      component = fixture.componentInstance;
      people$.next(people);
      selectedPerson$.next(people[0]);
      tasks$.next(taskItems);
      fixture.detectChanges();
    });
  }));

  it('should create', waitForAsync(() => {
    expect(component).toBeTruthy();
  }));

  it('should get the selected person', waitForAsync(() => {
    expect(component.newTask.personId).toEqual(people[0].id);
  }))

  it('should display the task description text box', waitForAsync(() => {
    fixture.whenStable()
    .then(() => {
      expect(fixture.debugElement.query(By.css('.task-description'))).toBeTruthy();
    });
  }));

  it('should set the value of the description when entered', waitForAsync(() => {
    let testValue = 'Testing: Something';
    let descriptionInput = fixture.debugElement.query(By.css('.task-description')).nativeElement;
    descriptionInput.value = testValue;
    descriptionInput.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    expect(component.newTask.description).toEqual(testValue);
  }));

  it('should show an error if the value entered is invalid', waitForAsync(() => {
    fixture.whenStable()
    .then(() => {
      let testValue = 'Testing';
      let descriptionInput = fixture.debugElement.query(By.css('.task-description')).nativeElement;
      descriptionInput.value = testValue
      descriptionInput.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(component.newTask.description).toEqual(testValue);
      expect(descriptionInput).toHaveClass('ng-invalid');
    });
  }));

  it('should not show an error if the value entered is valid', waitForAsync(() => {
    fixture.whenStable()
    .then(() => {
      let testValue = 'Testing: Something';
      let descriptionInput = fixture.debugElement.query(By.css('.task-description')).nativeElement;
      descriptionInput.value = testValue
      descriptionInput.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(descriptionInput).not.toHaveClass('ng-invalid');
    });
  }));

  it('should submit the task', waitForAsync(() => {
    fixture.whenStable()
    .then(() => {
      let descriptionValue = 'Testing';
      let statusValue = 'incomplete';
      let descriptionInput = fixture.debugElement.query(By.css('.task-description')).nativeElement;
      let statusInput = fixture.debugElement.query(By.css('.task-status')).nativeElement;
      let submitButton = fixture.debugElement.query(By.css('.submit-button')).nativeElement;
      descriptionInput.value = descriptionValue;
      descriptionInput.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(component.newTask.description).toEqual(descriptionValue);
      statusInput.click();
      statusInput.dispatchEvent(new Event('click'));
      fixture.detectChanges();
      let statusOption = fixture.debugElement.query(By.css('.option-one')).nativeElement;
      statusOption.click();
      statusOption.dispatchEvent(new Event('click'));
      expect(component.newTask.status).toEqual(statusValue);
      fixture.detectChanges();
      submitButton.click();
      submitButton.dispatchEvent(new Event('click'));
    });
  }));

  it('should save the task', waitForAsync(() => {
    component.newTask.description = "Testing 1";
    component.newTask.status = 'incomplete';
    component.newTask.personId = 1;
    component.submitTask();
    expectAsync(firstValueFrom(savedTask$)).toBeResolved();
  }));
});
