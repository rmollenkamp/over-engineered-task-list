export class TaskItem {
  id: number;
  personId: number;
  description: string;
  status: string;
}
