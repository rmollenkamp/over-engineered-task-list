import { Person } from "../_models/person";
/* istanbul ignore next */
export const people = <Person[]>[
  {
    id: 1,
    firstName: 'Test',
    lastName: 'Person 1'
  },
  {
    id: 2,
    firstName: 'Test',
    lastName: 'Person 2'
  },
  {
    id: 3,
    firstName: 'Test',
    lastName: 'Person 3'
  }
]
