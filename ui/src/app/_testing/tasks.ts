import { TaskItem } from "../_models/task"
/* istanbul ignore next */
export const taskItems = <TaskItem[]>[
  {
    id: 1,
    personId: 1,
    description: 'Test 1',
    status: 'incomplete'
  },
  {
    id: 2,
    personId: 2,
    description: 'Test 2',
    status: 'incomplete'
  },
  {
    id: 3,
    personId: 3,
    description: 'Test 3',
    status: 'complete'
  }
]
