import { HttpClient } from '@angular/common/http';
import { catchError, of, ReplaySubject, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { Person } from '../_models/person';
import { ErrorService } from './error.service';

@Injectable({
  providedIn: 'root'
})
export class PersonService {
  private baseUrl = 'http://localhost:3000';
  public people$: ReplaySubject<Person[]> = new ReplaySubject<Person[]>(1);
  public selectedPerson$: ReplaySubject<Person> = new ReplaySubject<Person>(1);

  constructor(
    private http: HttpClient,
    private errorService: ErrorService
  ){}

  getPeople(): void {
    const url = `${this.baseUrl}/people`;
    this.http.get<Person[]>(url)
    .pipe(
      catchError(err => {
        this.errorService.handleError('getPeople', err);
        return of(new Array<Person>())
      })
    ).subscribe({
      next: (people) => {
        this.people$.next(people);
        this.selectedPerson$.next(people[0])
      }
    });
  }
}
