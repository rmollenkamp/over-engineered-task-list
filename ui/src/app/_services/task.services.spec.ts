import { firstValueFrom, of } from "rxjs";
import { TaskServices } from "./task.services";
import { people } from "../_testing/people";
import { taskItems } from "../_testing/tasks";
import { waitForAsync } from "@angular/core/testing";
import { HttpErrorResponse } from "@angular/common/http";
import { ErrorService } from "./error.service";
import { TaskItem } from "../_models/task";

describe('TaskService', () => {
  const httpClientSpy = jasmine.createSpyObj('HttpClient', ['handler','request','delete','get','post']);
  const personServiceSpy = jasmine.createSpyObj('PersonService', ['getPeople']);
  const mockErrorService = <ErrorService>{
    handleError: (operation, error) => {}
  }

  let handleErrorSpy;

  const person = people[0];

  let taskService: TaskServices;

  beforeEach(() => {
    taskService = new TaskServices(httpClientSpy, personServiceSpy, mockErrorService);
    handleErrorSpy = spyOn(mockErrorService,'handleError');
    personServiceSpy.selectedPerson$ = of(person);
  })

  it('should create', waitForAsync(() => {
    expect(taskService).toBeTruthy();
  }));

  it('should return a list of tasks for Person 1', waitForAsync(() => {
    httpClientSpy.get.and.returnValue(of(taskItems));
    taskService.getTasks();
    expect(taskService.tasks$).toBeTruthy();
    expectAsync(firstValueFrom(taskService.tasks$)
    .then((tasks) => {
      expect(tasks.length).toEqual(1);
      expect(tasks[0].description).toEqual('Test 1')
    })).toBeResolved();
  }));

  it('should handle an error', waitForAsync(() => {
    httpClientSpy.post.and.returnValue(of(new HttpErrorResponse({status: 500, error: 'Testing', statusText: 'Internal Server Error'})));
    taskService.saveTask(new TaskItem());
    expect(handleErrorSpy).toHaveBeenCalled();
  }));
});
