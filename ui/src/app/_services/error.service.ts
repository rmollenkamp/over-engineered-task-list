import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { ErrorItem } from '../_models/error';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {
  private errors$: BehaviorSubject<ErrorItem[]> = new BehaviorSubject<ErrorItem[]>([]);

  constructor(){ /* TODO document why this constructor is empty */ }

  handleError(operation: string, error: string): void {
    let newError = new ErrorItem;
    newError.title = 'Error in ' + operation;
    newError.error = error;
    let newErrors = this.errors$.value;
    newErrors.push(newError);
    this.errors$.next(newErrors);
  }
}
