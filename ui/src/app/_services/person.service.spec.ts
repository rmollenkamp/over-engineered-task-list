import { of, take, tap } from "rxjs";
import { PersonService } from "./person.service";
import { people } from "../_testing/people";

import { ErrorService } from "./error.service";
import { HttpErrorResponse } from "@angular/common/http";
import { waitForAsync } from "@angular/core/testing";


describe('PersonService', () => {
  const httpClientSpy = jasmine.createSpyObj('HttpClient', ['handler','request','delete','get',]);
  const mockErrorService = <ErrorService>{
    handleError: (operation, error) => {}
  }
  const personService = new PersonService(httpClientSpy, mockErrorService);

  beforeEach(function(){
    spyOn(console, 'error');
  })

  it('should create', () => {
    let personService = new PersonService(httpClientSpy, mockErrorService);
    expect(personService).toBeTruthy();
  });

  it('should return a list of people', () => {
    httpClientSpy.get.and.returnValue(of(people));
    personService.getPeople();
    personService.people$
    .pipe(
      take(1),
      tap((ppl) => {
        ppl.forEach(p => {
          expect(p.lastName).toEqual('Person ' + p.id)
        })
      })
    ).subscribe();
  });
});
