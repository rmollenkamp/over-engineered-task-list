import { firstValueFrom } from "rxjs";
import { ErrorService } from "./error.service";
import { ErrorItem } from "../_models/error";
import { waitForAsync } from "@angular/core/testing";

describe('ErrorService', () => {
  const errorService = new ErrorService();

  it('should create', () => {
    expect(errorService).toBeTruthy();
  });

  it('should log an error', waitForAsync(() => {
    errorService.handleError('Test', 'Error');
    expectAsync(firstValueFrom(errorService['errors$'])).toBeResolved();
  }));
});
