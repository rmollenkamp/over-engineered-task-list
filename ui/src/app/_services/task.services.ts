import { TaskItem } from '../_models/task';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError, combineLatest, concatMap, filter, iif, of, ReplaySubject, tap, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { PersonService } from './person.service';
import { ErrorService } from './error.service';
import { Person } from '../_models/person';
const fs = require('fs');

@Injectable({
  providedIn: 'root'
})
export class TaskServices {
  private baseUrl = 'http://localhost:3000';
  public tasks$: ReplaySubject<TaskItem[]> = new ReplaySubject<TaskItem[]>(1);

  constructor(
    private http: HttpClient,
    private personService: PersonService,
    private errorService: ErrorService
  ){}

  getTasks(): void {
    const url = `${this.baseUrl}/tasks`;
    const taskItems$ = this.http.get<TaskItem[]>(url);
    combineLatest<[TaskItem[],Person]>(
      [
        taskItems$
        .pipe(
          concatMap((response) => iif(
            () => (response instanceof HttpErrorResponse),
            throwError(() => new Error(response['error'])),
            of(response)
          )),
          catchError((err) => {
            this.errorService.handleError('getTasks',err);
            return of(new Array<TaskItem>())
          })
        ),
        this.personService.selectedPerson$
      ]
    ).pipe(
      tap(([tasks, person]) => {
        this.tasks$.next(tasks.filter(t => t.personId === person.id && t.status === 'incomplete'))
      })
    ).subscribe();
  }

  saveTask(task: TaskItem): void {
    task.status = task.status || "another incomplete task that needs resolution"
    const url = `${this.baseUrl}/save-task`;
    this.http.post(
      url,
      task,
      { responseType: 'text' }
    ).pipe(
      concatMap((response: string | HttpErrorResponse) => iif(
        () => (response instanceof HttpErrorResponse),
        throwError(() => new Error(response['error'])),
        of(response)
      )),
      catchError((err) => {
        this.errorService.handleError('saveTask',err);
        return of('');
      }),
      tap(() => this.getTasks())
    ).subscribe();
  }

  completeTask(taskId: string): void {
    const url = `${this.baseUrl}/complete-task`;
    let params: HttpParams = new HttpParams();
    let headers = new HttpHeaders();
    params = params.append('taskId', taskId);
    this.http.post(url, null, {
      headers: headers,
      params: params,
      responseType: 'text'
    }).pipe(
      concatMap((response: string | HttpErrorResponse) => iif(
        () => (response instanceof HttpErrorResponse),
        throwError(() => new Error(response['error'])),
        of(response)
      )),
      catchError((err) => {
        this.errorService.handleError('completeTask', err);
        return of('');
      }),
      tap(() => this.getTasks())
    ).subscribe();
  }
}



