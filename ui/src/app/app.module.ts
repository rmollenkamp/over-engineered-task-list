import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {TaskServices} from './_services/task.services';
import { HttpClientModule } from '@angular/common/http';
import { TaskListComponent } from './task-list/task-list.component';
import { TaskComponent } from './task-list/task/task.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReactiveFormsTaskEntryComponent } from './task-entry/reactive-forms-task-entry/reactive-forms-task-entry.component';
import { TemplateDrivenTaskEntryComponent } from './task-entry/template-driven-task-entry/template-driven-task-entry.component';
import { PersonService } from './_services/person.service';
import { PersonSelectComponent } from './person-select/person-select.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatDialogModule } from '@angular/material/dialog';


@NgModule({
  declarations: [
    AppComponent,
    TaskListComponent,
    TaskComponent,
    ReactiveFormsTaskEntryComponent,
    TemplateDrivenTaskEntryComponent,
    PersonSelectComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatTabsModule,
    MatSelectModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
    NgbModule
  ],
  providers: [
    TaskServices,
    PersonService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
