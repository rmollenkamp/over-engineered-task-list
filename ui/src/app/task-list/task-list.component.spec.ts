import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { TaskListComponent } from './task-list.component';
import { TaskServices } from '../_services/task.services';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { TaskItem } from '../_models/task';
import { By } from '@angular/platform-browser';
import { TemplateDrivenTaskEntryComponent } from '../task-entry/template-driven-task-entry/template-driven-task-entry.component';
import { ReactiveFormsTaskEntryComponent } from '../task-entry/reactive-forms-task-entry/reactive-forms-task-entry.component';

describe('TaskListComponent', () => {
  let fixture: ComponentFixture<TaskListComponent>;
  let component: any;

  let tasks = [<TaskItem>{ id: 1, personId: 2, description: 'Test 1', status: 'incomplete'}, <TaskItem>{ id: 2, personId: 1, description: 'Test 2', status: 'complete'}]

  let taskServiceSpy = jasmine.createSpy('TaskServices');

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        NoopAnimationsModule,
        MatDialogModule
      ],
      declarations: [
        TaskListComponent
      ],
      providers: [
        {
          provide: TaskServices,
          use: taskServiceSpy
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents()
    .then(() => {
      fixture = TestBed.createComponent<TaskListComponent>(TaskListComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display a task item', waitForAsync(() => {
    component.taskService.tasks$.next(tasks);
    fixture.detectChanges();
    fixture.whenStable()
    .then(() => {
      let items: DebugElement[] = fixture.debugElement.queryAll(By.css('.list-item'));
      expect(items.length).toBe(1);
    });
  }));

  it('should display "No Tasks to Display" when the task list is empty', waitForAsync(() => {
    component.taskService.tasks$.next([]);
    fixture.detectChanges();
    fixture.whenStable()
    .then(() => {
      let div: HTMLElement = fixture.debugElement.query(By.css('.no-tasks'))?.nativeElement;
      expect(div.innerHTML).toEqual('No Tasks to Display');
    });
  }));

  it('should default the form type to reactive', () => {
    expect(component.formType$.value).toEqual('reactive');
  });

  it('should set the form type to template', () => {
    component.setForms({index: 1});
    expect(component.formType$.value).toEqual('template');
  });

  it('should set the form type back to reactive', () => {
    component.setForms({index: 1});
    component.setForms({index: 0});
    expect(component.formType$.value).toEqual('reactive');
  });

  it('should open the reactive task entry component', () => {
    spyOn(component.dialog,'open')
    component.setForms({index: 0});
    component.showModal();
    expect(component.dialog.open).toHaveBeenCalledWith(ReactiveFormsTaskEntryComponent,{width: '300px'});
  });

  it('should open the template driven task entry component', () => {
    spyOn(component.dialog,'open')
    component.setForms({index: 1});
    component.showModal();
    expect(component.dialog.open).toHaveBeenCalledWith(TemplateDrivenTaskEntryComponent,{width: '300px'});
  });

  it('should not open a dialog if no form type is selected', () => {
    spyOn(component.dialog,'open')
    component.formType$.next(undefined);
    component.showModal();
    expect(component.dialog.open).not.toHaveBeenCalled();
  });
});
