import { Component } from "@angular/core";
import { TaskServices } from "../_services/task.services";
import { BehaviorSubject } from "rxjs";
import { MatTabChangeEvent } from "@angular/material/tabs";
import { MatDialog } from "@angular/material/dialog";
import { TemplateDrivenTaskEntryComponent } from "../task-entry/template-driven-task-entry/template-driven-task-entry.component";
import { ReactiveFormsTaskEntryComponent } from "../task-entry/reactive-forms-task-entry/reactive-forms-task-entry.component";
import { FormType } from "../_enums/formType";

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css'],
})
export class TaskListComponent {
  formType$: BehaviorSubject<string> = new BehaviorSubject<string>(FormType[FormType.reactive]);

  constructor(
    protected taskService: TaskServices,
    private dialog: MatDialog
  ) {
    this.loadTasks();
  }

  loadTasks(): void {
    this.taskService.getTasks();
  }

  setForms(event: MatTabChangeEvent): void {
    this.formType$.next(FormType[event.index]);
  }

  showModal(): void{
    switch(this.formType$.value){
      case 'template':
        this.dialog.open(TemplateDrivenTaskEntryComponent, {
          width: '300px'
        })
        break;
      case 'reactive':
        this.dialog.open(ReactiveFormsTaskEntryComponent, {
          width: '300px'
        })
        break;
      default:
        break;
    }
  }
}
