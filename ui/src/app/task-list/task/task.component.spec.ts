import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TaskComponent } from './task.component';
import { TaskItem } from 'src/app/_models/task';
import { TaskServices } from 'src/app/_services/task.services';

describe('TaskComponent', () => {
  let fixture: ComponentFixture<TaskComponent>;
  let component: any;

  let task = <TaskItem>{ id: 1, personId: 2, description: 'Test 1', status: 'incomplete'};

  let taskServiceSpy = jasmine.createSpy('TaskServices');

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        NoopAnimationsModule
      ],
      providers: [
        {
          provide: TaskServices,
          use: taskServiceSpy
        }
      ],
      declarations: [
        TaskComponent
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents()
    .then(() => {
      fixture = TestBed.createComponent<TaskComponent>(TaskComponent);
      component = fixture.componentInstance;
      component.task = task;
    });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a task description of "Test 1"', () => {
    expect(component.task.description).toEqual('Test 1');
  });

  it('should mark the task as complete', () => {
    component.markComplete();
    expect(component.task.status).toEqual('complete');
  });
});
