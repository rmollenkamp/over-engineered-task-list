import { Component, EventEmitter, Input, Output } from "@angular/core";
import { take } from "rxjs";
import { TaskItem } from "src/app/_models/task";
import { TaskServices } from "src/app/_services/task.services";

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css'],
})
export class TaskComponent {
  @Input() task: TaskItem;

  @Output() newTaskEvent: EventEmitter<void> = new EventEmitter<void>();

  constructor(
    private taskService: TaskServices
  ) { /* TODO document why this constructor is empty */ }

  markComplete(): void {
    this.task.status = 'complete';
    this.taskService
    .completeTask(this.task.id.toString());
  }
}
