import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { By } from '@angular/platform-browser';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let component: any;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents()
    .then(() => {
      fixture = TestBed.createComponent<AppComponent>(AppComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });
  }));

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it(`should have as title 'Over-Engineered Task List'`, () => {
    expect(component.title).toEqual('Over-Engineered Task List');
  });

  it('should render title', () => {
    const element: HTMLElement = fixture.debugElement.query(By.css('.title')).nativeElement
    expect(element.innerHTML).toContain('Over-Engineered Task List');
  });
});
